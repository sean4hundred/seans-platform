﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public float speedForce = 0.5f;
	public Rigidbody2D rigidbody2D;
	public Vector2 jumpVector;
	public bool grounded;
	public Transform grounder;
	public float radiuss;
	public float jumpHeight;

	public LayerMask ground;
	private Animator animator;

	// Use this for initialization
	void Start () {
		rigidbody2D = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey(KeyCode.A)){
			rigidbody2D.velocity = new Vector2(-speedForce,rigidbody2D.velocity.y);
			transform.localScale = new Vector3(-1,1,1);
			animator.SetTrigger("player-idle");
		}else if(Input.GetKey(KeyCode.D)){
			rigidbody2D.velocity = new Vector2(+speedForce,rigidbody2D.velocity.y);
			transform.localScale = new Vector3(1,1,1);
			animator.SetTrigger("player-idle");
		}else{
			if(Input.GetKeyDown (KeyCode.Space) && grounded) 
			{
				Jump();
				animator.SetTrigger("jump");

			}
			
			if (Input.GetKeyDown (KeyCode.Space) && !grounded) 
			{
				Jump();
			}


			}
		}
	public void Jump ()
	{
		rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, jumpHeight);
	}
	}

